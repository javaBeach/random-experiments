package org.tbuttler.sim;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class SampleFactory {

    public Sample pullSample(Population population, IntStream sampleIndices) {
        List<Element> sample = sampleIndices
                .mapToObj(population::getElement)
                .collect(Collectors.toList());
        return new Sample(sample);
    }
}
