package org.tbuttler.sim;

import java.util.List;

import lombok.Data;

@Data
public class Sample {

    private final List<Element> elements;

    public boolean hasDefect() {
        return elements.stream().anyMatch(e -> e.getType().equals(Element.Status.INVALID));
    }
}
