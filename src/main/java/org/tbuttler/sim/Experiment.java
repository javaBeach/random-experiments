package org.tbuttler.sim;

import java.util.stream.IntStream;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class Experiment {

    private final SampleFactory sampleFactory;

    Boolean runExperiment(Population population, IntStream sampleIndices) {
        Sample sample = sampleFactory.pullSample(population, sampleIndices);
        return sample.hasDefect();
    }
}
