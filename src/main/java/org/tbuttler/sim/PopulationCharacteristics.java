package org.tbuttler.sim;

import lombok.Data;

@Data
public class PopulationCharacteristics {

    private final int populationSize;

    private final int numberOfInvalid;

}
