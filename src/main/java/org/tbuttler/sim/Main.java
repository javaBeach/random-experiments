package org.tbuttler.sim;

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        System.out.println("Start simulation");

        SimulationDesign simulationDesign = SimulationDesign.builder()
                .populationCharacteristics(new PopulationCharacteristics(100000, 100))
                .sampleSize(3000)
                .numberOfRuns(10000)
                .build();
        System.out.println("Simulation Design: " + simulationDesign);
        Simulation simulation = new Simulation(simulationDesign);
        long successfulRuns = simulation.run(new Random(43798L));

        System.out.println(String.format("Defect found %d out of %d times", successfulRuns, simulationDesign.getNumberOfRuns()));
        // - check whether at least one item found
        // - repeat n times: how many times did we meet the check
    }


}