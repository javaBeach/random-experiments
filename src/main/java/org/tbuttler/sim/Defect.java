package org.tbuttler.sim;

import lombok.Data;

@Data
public class Defect {

    private final String description;
}
