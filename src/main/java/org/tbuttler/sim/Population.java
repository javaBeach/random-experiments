package org.tbuttler.sim;

import java.util.List;

import lombok.Data;

@Data
public class Population {

    private final List<Element> elements;

    public int getSize() {
        return elements.size();
    }

    public Element getElement(int index) {
        return elements.get(index);
    }
}
