package org.tbuttler.sim;

import java.util.Collections;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Simulation {

    private SimulationDesign design;

    public Simulation(SimulationDesign design) {
        this.design = design;
    }

    public long run(Random randomization) {
        Experiment experiment = new Experiment(new SampleFactory());
        Set<Integer> indicesOfInvalids = indicesOfInvalids(design.getPopulationCharacteristics(), randomization);
        Population population = new PopulationFactory().generate(design.getPopulationCharacteristics(), indicesOfInvalids);

        //run the experiment:
        return IntStream.range(0, design.getNumberOfRuns())
                .mapToObj(i -> experiment.runExperiment(population, nextSampleIndices(randomization)))
                .filter(Boolean.TRUE::equals)
                .count();
    }

    private IntStream nextSampleIndices(Random random) {
        return random.ints(design.getSampleSize(), 0, design.getPopulationCharacteristics().getPopulationSize());
    }

    private Set<Integer> indicesOfInvalids(PopulationCharacteristics design, Random random) {
        Set<Integer> indices = Collections.emptySet();
        //check that the set contains the required number of elements
        while (design.getNumberOfInvalid() != indices.size()) {
            indices = random.ints(design.getNumberOfInvalid(), 0, design.getPopulationSize())
                    .mapToObj(Integer::valueOf) //stream might contain duplicates here
                    .collect(Collectors.toSet());
        }
        return indices;
    }
}
