package org.tbuttler.sim;


import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public class PopulationFactory {

    public Population generate(PopulationCharacteristics simulationDesign, Set<Integer> indicesOfInvalids) {
        List<Element> elements = createElements2(simulationDesign, indicesOfInvalids);
        return new Population(elements);
    }

    private List<Element> createElements2(PopulationCharacteristics design, Set<Integer> indicesOfInvalids) {
        Element[] elements = new Element[design.getPopulationSize()];
        Arrays.fill(elements, new Element(Element.Status.VALID, Collections.emptySet()));

        for (Integer i : indicesOfInvalids) {
            flip(elements, i);
        }
        return Arrays.asList(elements);
    }

    private Element flip(Element[] elements, int index) {
        elements[index] = new Element(Element.Status.INVALID, createDefects());
        return elements[index];
    }

    private Set<Defect> createDefects() {
        Defect defectOne = new Defect("defect 1");
        Defect defectTwo = new Defect("defect 2");

        return Set.of(defectOne, defectTwo);
    }


}
