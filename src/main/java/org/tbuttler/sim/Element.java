package org.tbuttler.sim;

import java.util.Set;

import lombok.Data;

@Data
public class Element {

    public enum Status {
        INVALID, VALID
    }

    private final Status type;

    private final Set<Defect> defects;
}
