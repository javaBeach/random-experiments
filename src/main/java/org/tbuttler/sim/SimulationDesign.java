package org.tbuttler.sim;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SimulationDesign {

    private final PopulationCharacteristics populationCharacteristics;


    private final int sampleSize;

    private final int numberOfRuns;
}
