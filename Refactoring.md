
# Ideas

## Create a simulation class
* SimulationDesign: describes population characteristics, and sampling strategy
* Simulation: create population, and run the experiment

## Change the experiment
* We could have different strategies for creating defects
* We could have different evaluation methods for the experiment
  * Evaluate whether at least one defect is found
  * Evaluate whether all defect types are found
* We should be able to seed the defects, not to keep them as constants
  * how can we create a valid population that complies with the population characteristics?

## what would I need to do to create a cleaner functional design
* Separation of data, calculation, action?

## Timeline
* create RANDOM
* create PopulationFactory
* create SimulationDesign
* create population via factory
* create sampling instructions (list of indices?)
* population: sample
* 